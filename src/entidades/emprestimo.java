package entidades;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedro
 */
public class emprestimo {
    int tipo_emprestimo;
    double valor;
    int tempo;
    int valorparcelas;

    public int getTipo_emprestimo() {
        return tipo_emprestimo;
    }

    public void setTipo_emprestimo(int tipo_emprestimo) {
        this.tipo_emprestimo = tipo_emprestimo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public int getValorparcelas() {
        return valorparcelas;
    }

    public void setValorparcelas(int valorparcelas) {
        this.valorparcelas = valorparcelas;
    }
}
