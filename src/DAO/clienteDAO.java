/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import conexao.Conexao;
import entidades.clientes;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author daviondejesus
 */
public class clienteDAO {
    private final Conexao conexao;
    
    public clienteDAO() {
	this.conexao = new Conexao();
    }

    public boolean gravarCliente(clientes cliente) {
	try {
            String sql = "INSERT INTO cliente "
                       + "(nome, "
                       + "endereco, "
                       + "cpf, "
                       + "cidade, "
                       + "nascimento, "
                       + "estado, "
                       + "civil, "
                       + "sexo, "
                       + "telefone, "
                       + "renda) " 
                       + "VALUES (?,?,?,?,?,?,?,?,?,?)";
			
            PreparedStatement stm = conexao.getConexao().prepareStatement(sql);
            stm.setString(1, cliente.getNome_cliente());
            stm.setString(2, cliente.getEndereco());
            stm.setString(3, cliente.getCpf());
            stm.setString(4, cliente.getCidade());
            stm.setString(5, cliente.getData_nascimento());
            stm.setString(6, cliente.getEstado());
            stm.setString(7, cliente.getEstado_civil());
            stm.setString(8, cliente.getSexo());
            stm.setString(9, cliente.getTelefone());
            stm.setDouble(10, cliente.getRenda());

            stm.execute();
            conexao.getConexao().commit();
            conexao.desconecta();
            return true;
			
	} catch (SQLException e) {
            System.out.println("Erro: " + e);
	} finally{
            conexao.desconecta();
        }

	return false;
    }
}
