/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import conexao.Conexao;
import entidades.emprestimo;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 *
 * @author pedro
 */
public class emprestimoDAO {
    private final Conexao conexao;
    
    public emprestimoDAO() {
	this.conexao = new Conexao();
    }

    public boolean gravarEmprestimo(emprestimo Emprestimo, int idcliente, double totalemprestimo) {
	try {
            String sql = "INSERT INTO emprestimo "
                       + "(tipo_emprestimo, "
                       + "valor_emprestimo, "
                       + "tempo_pagamento, "
                       + "id_cliente, "
                       + "valor_total) " 
                       + "VALUES (?,?,?,?,?)";
			
            PreparedStatement stm = conexao.getConexao().prepareStatement(sql);
            stm.setInt(1, Emprestimo.getTipo_emprestimo());
            stm.setDouble(2, Emprestimo.getValor());
            stm.setInt(3, Emprestimo.getTempo());
            stm.setInt(4, idcliente);
            stm.setDouble(5, totalemprestimo);
            

            stm.execute();
            conexao.getConexao().commit();
            conexao.desconecta();
            return true;
			
	} catch (SQLException e) {
            System.out.println("Erro: " + e);
	} finally{
            conexao.desconecta();
        }

	return false;
    }
}
