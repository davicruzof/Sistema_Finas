package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author daviondejesus
 */
public class Conexao {

	private final String driver = "org.sqlite.JDBC";
	private final String url = "jdbc:sqlite:db/dbFinas.db";
	private Connection conexao = null;
	
	public Conexao(){
		try {
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConexao(){
		if(conexao == null){
			try {
				conexao = DriverManager.getConnection(url);
				conexao.setAutoCommit(false);
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
		return conexao;
	}
	
	public void desconecta(){
		try {
                    conexao.close();
		} catch (SQLException e) {
                    e.printStackTrace();
		}
		conexao = null;
	}
}
